import os
import os.path
import sys
import pkgutil

paths_schema = {}
urls_schema = {}

default_schema = 'default'
default_relative = None

def loadTranslations(schema=default_schema):
    
    paths = []
    urls = []
    
    if schema is None:
        schema='default'
    
    if schema in list(paths_schema.keys()):
         # do not reload if already loaded
        paths = paths_schema[schema]
        urls = urls_schema[schema]
    else:
        # not cached
        translationFile = "etc/pathTranslation_%s.conf" % schema
        try:
            translate = pkgutil.get_data('pathurl', translationFile)
        except IOError as e:
            raise RuntimeError("schema %s not found : %s" % (schema, str(e)))
        translate = translate.decode("utf-8")
        lines = translate.splitlines()
        for line in lines:
            splt = line.split(" ")
            paths.append(os.path.expandvars(splt[0].strip()))
            urls.append(splt[1].strip())
        paths_schema[schema] = paths
        urls_schema[schema] = urls
            
    return paths, urls



def toUrl(path, **kwargs): 
    
    if 'schema' not in kwargs:
        kwargs['schema'] = default_schema
    if 'relative' not in kwargs:
        kwargs['relative'] = default_relative
        
    relative = kwargs['relative']
    schema = kwargs['schema']
    
    pathsToUse,urlsToUse = loadTranslations(schema)
        
    url = path
    for i in range(len(pathsToUse)):
        url = url.replace(pathsToUse[i], urlsToUse[i])

    if '://'  not in url:
        url="file://%s" % path
        
    if relative is not None:
        url_from=toUrl(relative, relative=None, schema=schema)  
        url_rel=os.path.relpath(url, url_from)
        url = url_rel
        
              
    return url

def toPath(url, **kwargs): 
    
    if 'schema' not in kwargs:
        kwargs['schema'] = default_schema
    if 'relative' not in kwargs:
        kwargs['relative'] = default_relative
        
    relative = kwargs['relative']
    schema = kwargs['schema']
    
    pathsToUse,urlsToUse = loadTranslations(schema)
    
    path = url
    for i in range(len(urlsToUse)):
        path = path.replace(urlsToUse[i], pathsToUse[i])
        
    if relative is not None:
        path_from=toPath(relative, relative=None, schema=schema)  
        path_rel=os.path.relpath(path, path_from)
        path = path_rel

    return path.replace("file://","")
